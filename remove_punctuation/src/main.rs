// Import necessary modules
use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};

// Define the input structure
#[derive(Deserialize, Serialize)]
struct Input {
    lowercase: String, // Input text to be processed (converted to lowercase)
}

// Define the output structure
#[derive(Deserialize, Serialize)]
struct Output {
    result: String, // Processed output text
}

// Entry point of the Lambda function
#[tokio::main]
async fn main() -> Result<(), Error> {
    // Define the Lambda function handler
    let func = handler_fn(process_data);
    // Run the Lambda function
    lambda_runtime::run(func).await?;
    Ok(())
}

// Function to process the input data
async fn process_data(event: Input, _ctx: Context) -> Result<Output, Error> {
    // Initialize an empty string to store the processed output
    let mut result = String::new();
    
    // Iterate over each character in the input text
    for c in event.lowercase.chars() {
        // Check if the character is alphanumeric or whitespace
        if c.is_alphanumeric() || c.is_whitespace() {
            // If the character is alphanumeric or whitespace, append it to the result
            result.push(c);
        }
    }
    
    // Return the processed output wrapped in the Output struct
    Ok(Output { result })
}
