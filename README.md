# Individual Project 4 
[![pipeline status](https://gitlab.com/dukeaiml/IDS721/yc557-ip-4/badges/main/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/yc557-ip-4/-/commits/main)
> Oliver Chen (yc557)

## Project Introduction
In this project, we will develop a data processing pipeline using Rust AWS Lambda functions and AWS Step Functions. The pipeline will involve a few Lambda functions orchestrated by Step Functions to perform various tasks such as data preprocessing.

## Project Steps

### Rust Lambda Functionality
There are two Rust Lambda functionts in play here.

**Text to Lowercase**: This lambda function would take the input text and convert it to lowercase. This could be useful for standardizing the text and ensuring consistent processing downstream in the pipeline. The output would be the same text with all characters converted to lowercase.

**Remove Punctuation**: This lambda function would remove all punctuation marks from the input text. Punctuation marks such as periods, commas, and quotation marks might not be relevant to certain types of analysis and could be removed to focus only on the textual content. The output would be the input text without any punctuation marks.


1. Create two new Rust Lambda function using the follow commands:
```bash
cargo lambda new text_to_lowercase
cargo lambda new remove_punctuation
```
2. Implement functionality in each `main.rs` and add required dependencies in `Cargo.toml`.
3. After implementation, we test each Lambda function. 
```bash
# In root directory yc557-ip-4
cd text_to_lowercase
cargo lambda watch
# Open a new terminal
cargo lambda invoke --data-file ./data/data1.json

cd ../remove_punctuation
cargo lambda watch
# Open a new terminal
cargo lambda invoke --data-file ./data/data2.json
```
![image](./images/6.png)
![image](./images/7.png)

4. Lastly, we deploy the Lambda functions onto AWS using the following commands. We do this for both the Lambda functions. We can also test the Lambda functions on AWS.
```bash
cargo lambda build --release
cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::471112660632:role/ids721
```
![image](./images/1.png)
![image](./images/2.png)
![image](./images/3.png)
![image](./images/4.png)


### Step Functions Workflow
1. Log in to AWS console and create a new Step Function.
2. Build the state machine workflow according to the Lambda functions. 
```json
{
  "Comment": "This state machine transforms input text to lowercase and removes punctuation.",
  "StartAt": "TextToLowercase",
  "States": {
    "TextToLowercase": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-1:471112660632:function:text_to_lowercase",
      "Next": "RemovePunctuation"
    },
    "RemovePunctuation": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-1:471112660632:function:remove_punctuation",
      "End": true
    }
  }
}
```
![image](./images/5.png)

3. Use the following input data to start execution:
```json
{
    "data": "LIfe iS A jOurneY, nOt a deStInAtIon. iT's NOt abOUt rEachIng tHe eNd bUt rAthEr aBOUt embRaCIng tHe eXperIenCes, lesSOns, And gROwTh alOng tHe waY. eACh mOment iS An oppOrtunItY fOr leArning And sElf-dIsCOverY. chAllEnges mAy ArISe, bUt thEy ArE stEpPing stOnes tO resIlIenCe And wiSdOm. reMEmbEr tO chEriSh tHe preSent, fOr iT iS whEre trUe hAppIneSs reSidEs."
}
```
![image](./images/8.png)
![image](./images/10.png)

### Data Processing Pipeline
In the `TextToLowercase` stage, the first Lambda function would take the input text named `data` and convert it to lowercase. After that, in the `RemovePunctuation` stage, the second Lambda function takes the previous output named `lowercase` and remove all punctuation marks from that text. This forms a simple data processing pipeline. The following picture encapsulates it well. The `lowercase` data is after the first stage and before the second stage. We can see there are no capitalized letters while punctuation still exists.
![image](./images/9.png)

## Demo Video
![demo](./images/demo.mov)

Or you can access the demo video [here](./images/demo.mov) in the repo.
